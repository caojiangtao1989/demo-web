// 配置参考 ： https://cli.vuejs.org/zh/config/#devserver
module.exports={

    // publicPath: process.env.NODE_ENV === 'production'? '/demo/' : '/',
    publicPath: '/',
    outputDir: 'dist',

    devServer: {
        port: 9009, 
        // proxy: {
        //     '/api': 'http://localhost:3000',
        // },
    },

    // 修改index.html中的标题名称
    chainWebpack: config => {
        config.plugin('html').tap(
            args => {
                args[0].title = '测试demo';
                return args; 
            }
        )
    }

}